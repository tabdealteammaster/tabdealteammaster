//
//  AppDelegate.m
//  TabDeal
//
//  Created by Sreeraj VR on 17/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "AppDelegate.h"
#import <FacebookSDK/FacebookSDK.h>

#import <Fabric/Fabric.h>
#import <TwitterKit/TwitterKit.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [self initializer];
    [FBLoginView class];
    [FBProfilePictureView class];
    
    [Fabric with:@[TwitterKit]];
    
    
    FBSession* session = [FBSession activeSession];
    [session closeAndClearTokenInformation];
    [session close];
    [FBSession setActiveSession:nil];
    
    NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray* facebookCookies = [cookies cookiesForURL:[NSURL         URLWithString:@"https://facebook.com/"]];
    
    for (NSHTTPCookie* cookie in facebookCookies) {
        [cookies deleteCookie:cookie];
    }
    // Override point for customization after application launch.
    
    self.launchViewObj = [[LaunchingViewController alloc] initWithNibName:@"LaunchingViewController" bundle:nil];
    self.window = [[UIWindow alloc] initWithFrame:
                   [[UIScreen mainScreen] bounds]];
    UINavigationController *navigation = [[UINavigationController alloc]initWithRootViewController:self.launchViewObj];
    self.HUD = [[MBProgressHUD alloc] initWithView:self.launchViewObj.view];
    [ApplicationDelegate.HUD.delegate self];
    
    self.window.rootViewController = navigation;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    // Call FBAppCall's handleOpenURL:sourceApplication to handle Facebook app responses
    BOOL wasHandled = [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
    
    // You can add your app-specific url handling code here if needed
    
    return wasHandled;
}

-(BOOL)isValid:(id)sender
{
    BOOL valid = NO;
    if ((sender!=nil)&&(![sender isEqual:[NSNull null]]))
    {
        valid=YES;
    }
    return valid;
}

-(BOOL)checkNetworkAvailability
{
    BOOL status;
    Reachability *network = [Reachability reachabilityForInternetConnection];
    NetworkStatus statusOfConnection = [network currentReachabilityStatus];
    if (statusOfConnection == NotReachable)
    {
        status = NO;
    }
    else
    {
        status = YES;
    }
    return status;
}

-(void)initializer
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    internetReachable = [Reachability reachabilityForInternetConnection];
    [internetReachable startNotifier];
    
    self.engine = [[AppEngine alloc] initWithHostName:kHOST_URL customHeaderFields:nil];
    [self.engine emptyCache];
    [self.engine useCache];
    self.mapper = [[ObjectMapper alloc] init];
}

#pragma mark -
-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
    
    if (internetStatus == NotReachable)
    {
        UIAlertView *erroralert = [[UIAlertView alloc] initWithTitle:localize(@"Message") message:localize(@"Sorry, network appears to be offline. Please try later.") delegate:nil cancelButtonTitle:localize(@"Okay") otherButtonTitles:nil, nil];
        [erroralert show];
    }
    else
    {
        
    }
}

-(void)addProgressHUDToView:(UIView *)parentview
{
    if ([ApplicationDelegate.HUD superview]) {
        [ApplicationDelegate.HUD removeFromSuperview];
    }
    ApplicationDelegate.HUD.center = parentview.center;
    [parentview addSubview:ApplicationDelegate.HUD];
    [ApplicationDelegate.HUD removeFromSuperViewOnHide];
    [ApplicationDelegate.HUD show:YES];
}
-(void)removeProgressHUD
{
    if ([ApplicationDelegate.HUD superview]) {
        [ApplicationDelegate.HUD hide:YES];
    }
}

-(void)showAlertWithMessage:(NSString *)message title:(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle: title
                              message: message
                              delegate: nil
                              cancelButtonTitle: @"Okay"
                              otherButtonTitles: nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        //Show alert here
        [alertView show];
    });
    //[alertView show];
}
@end
