//
//  LoginViewController.m
//  TabDeal
//
//  Created by satheesh on 9/17/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "LoginViewController.h"
#import "CreateNewAccountViewController.h"
#import "AppDelegate.h"
#import "UserData.h"
#import <TwitterKit/TwitterKit.h>



@interface LoginViewController (){
    CreateNewAccountViewController* createNewAccObj;
    UINavigationController * loginNavigation;
    UIViewController *mainView;
    HomeViewController * homePageViewObj;
}

@end

@implementation LoginViewController

-(void)viewWillAppear:(BOOL)animated{
    //NSMutableDictionary *postFbDic;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    TWTRLogInButton *logInButton = [TWTRLogInButton buttonWithLogInCompletion:^(TWTRSession *session, NSError *error) {
    if (session) {
    NSLog(@"signed in as %@", [session userName]);
    NSLog(@"%@",[session userID]);
    
        homePageViewObj = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
        [self.navigationController pushViewController:homePageViewObj animated:NO];
    
    if ([session userID]==nil) {
    [self.postTweetDic setObject:@"" forKey:@"Id"];
    } else {
    [self.postTweetDic setObject:[session userID] forKey:@"Id"];
    }
    if ([session userName]==nil) {
    [self.postTweetDic setObject:@"" forKey:@"first_name"];
    } else {
    [self.postTweetDic setObject:[session userName] forKey:@"FirstName"];
    }
    if ([session userName]==nil) {
    [self.postTweetDic setObject:@"" forKey:@"last_name"];
    } else {
    [self.postTweetDic setObject:[session userName] forKey:@"LastName"];
    }
    if ([session userID]==nil) {
    [self.postTweetDic setObject:@"" forKey:@"name"];
    } else {
    [self.postTweetDic setObject:[session userID] forKey:@"UserID"];
    }

        [self LoginwithTwitter];
        
    }
    
    else
    {
    NSLog(@"error: %@", [error localizedDescription]);
    }
        
    }];
    
    logInButton.center = self.view.center;
    logInButton.frame = self.twitterBackVIew.frame;
    [self.LoginmainBackView addSubview:logInButton];

    self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    self.passwordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    
    self.emailIMg.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    self.passwordIMg.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    
    FBSession* session = [FBSession activeSession];
    [session closeAndClearTokenInformation];
    [session close];
    [FBSession setActiveSession:nil];
    
    NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray* facebookCookies = [cookies cookiesForURL:[NSURL         URLWithString:@"https://facebook.com/"]];
    
    for (NSHTTPCookie* cookie in facebookCookies) {
        [cookies deleteCookie:cookie];
    }
    
    self.navigationController.navigationBarHidden = YES;
    self.loginBackView.layer.cornerRadius = 4;
    self.emailBackView.layer.cornerRadius = 3;
    self.passwordBackView.layer.cornerRadius = 3;
    self.loginBttn.layer.cornerRadius = 3;
    self.registerBttn.layer.cornerRadius = 3;
    
    self.emailTextView.delegate = self;
    self.passwordTextView.delegate=self;
    [self.keepMeSignInBttn setBackgroundImage:[UIImage imageNamed:@"Tickbox_Unselected.png"] forState:UIControlStateNormal];
    [self.keepMeSignInBttn setBackgroundImage:[UIImage imageNamed:@"Tickbox_Selected.png"] forState:UIControlStateSelected];
    
    userDataStore = [NSUserDefaults standardUserDefaults];
    if ([[userDataStore objectForKey:@"keepMeLogin"]isEqualToString:@"Ticked"]) {
        self.keepMeSignInBttn.selected = YES;
    } else {
        [self.keepMeSignInBttn setSelected:NO];
    }
    
    [self.signinwithFBBttn setReadPermissions:@[@"public_profile"]];
    [self.signinwithFBBttn setDelegate:self];
    
    self.postFbDic = [[NSMutableDictionary alloc] init];
    self.postTweetDic = [[NSMutableDictionary alloc] init];
    
    // [[Localization sharedInstance] setPreferred:@"ar" fallback:@"en"];
    
    [[Localization sharedInstance] setPreferred:@"en" fallback:@"ar"];
    
    [self.emailHeadingLbl setText:localize(@"Email")];

    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)registerBtnActn:(UIButton *)sender {
    createNewAccObj = [[CreateNewAccountViewController alloc]initWithNibName:@"CreateNewAccountViewController" bundle:nil];
    [self.navigationController pushViewController:createNewAccObj animated:NO];
 }

- (IBAction)loginBttnActn:(UIButton *)sender {
    if ((self.emailTextView.text.length == 0) && (self.passwordTextView.text.length == 0)) {
        NSLog(@"Please enter email id and password");
        alrtView = [[UIAlertView alloc]initWithTitle:@"Sorry..!" message:@"Please enter email id and password" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alrtView show];
    } else if(self.emailTextView.text.length == 0){
        NSLog(@"Please enter email");
        alrtView = [[UIAlertView alloc]initWithTitle:@"Sorry..!" message:@"Please enter email" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alrtView show];
    }else if(self.passwordTextView.text.length == 0){
        NSLog(@"Please enter password");
        alrtView = [[UIAlertView alloc]initWithTitle:@"Sorry..!" message:@"Please enter password" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alrtView show];
    }else{
        if ([self NSStringIsValidEmail:self.emailTextView.text]) {
            [self loginUser];
        } else {
            NSLog(@"Please enter valid email id");
            alrtView = [[UIAlertView alloc]initWithTitle:@"Sorry..!" message:@"Please enter valid email id" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alrtView show];
        }
    }
}

- (IBAction)keepMeSignedInBttnActn:(UIButton *)sender {
    self.keepMeSignInBttn.selected = !self.keepMeSignInBttn.selected;
    
}

- (IBAction)emailtext:(UITextField *)sender {
    self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_SELECTED_C0lOR];
    self.passwordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    self.emailIMg.image = [UIImage imageNamed:@"user_sel-1.png"];
    self.passwordIMg.image = [UIImage imageNamed:@"password-2.png"];
}


-(NSMutableDictionary *)getUserSignInPostData
{
    
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:self.emailTextView.text forKey:@"Email"];
    [postDic setObject:self.passwordTextView.text forKey:@"Password"];
    
    return postDic;
}



#pragma NSURLConnection Delegates

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    if (!self.receivedData){
        self.receivedData = [NSMutableData data];
    }
    [self.receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    NSString *responseString = [[NSString alloc] initWithData:self.receivedData encoding:NSUTF8StringEncoding];
    NSLog(@"%@",responseString);
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user{
    
    if ([user objectForKey:@"mobile"]==nil) {
        [self.postFbDic setObject:@"" forKey:@"MobileNumber"];
    } else {
        [self.postFbDic setObject:[user objectForKey:@"mobile"] forKey:@"MobileNumber"];
    }
    if ([user objectForKey:@"email"]==nil) {
        [self.postFbDic setObject:@"" forKey:@"Email"];
    } else {
        [self.postFbDic setObject:[user objectForKey:@"email"] forKey:@"Email"];
    }
    if ([user objectForKey:@"id"]==nil) {
        [self.postFbDic setObject:@"" forKey:@"Id"];
    } else {
        [self.postFbDic setObject:[user objectForKey:@"id"] forKey:@"Id"];
    }
    if ([user objectForKey:@"first_name"]==nil) {
        [self.postFbDic setObject:@"" forKey:@"first_name"];
    } else {
        [self.postFbDic setObject:[user objectForKey:@"first_name"] forKey:@"FirstName"];
    }
    if ([user objectForKey:@"last_name"]==nil) {
        [self.postFbDic setObject:@"" forKey:@"last_name"];
    } else {
        [self.postFbDic setObject:[user objectForKey:@"last_name"] forKey:@"LastName"];
    }
    if ([user objectForKey:@"name"]==nil) {
        [self.postFbDic setObject:@"" forKey:@"name"];
    } else {
        [self.postFbDic setObject:[user objectForKey:@"name"] forKey:@"UserName"];
    }

    NSLog(@"%@",self.postFbDic);
    [self LoginwithFacebook];
    

}

-(void)sendFbQuery{
    urlFb = [NSURL URLWithString:@"http://tabdeal.mawaqaademo.com/service/API.svc/FacebookOrtwitterSignIn"];
    theRequestFb = [NSMutableURLRequest requestWithURL:urlFb];
    
    NSMutableDictionary *loginData = [[NSMutableDictionary alloc] initWithDictionary:self.postFbDic];
    NSError * error = nil;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:loginData options:0 error:&error];
    NSString *resultAsString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSLog(@"%@",resultAsString);
    NSString *msgLength = [NSString stringWithFormat:@"%d", [resultAsString length]];
    [theRequestFb addValue: @"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequestFb addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequestFb setHTTPMethod:@"POST"];
    [theRequestFb setHTTPBody: [resultAsString dataUsingEncoding:NSUTF8StringEncoding]];
    
    self.connectionFb =[[NSURLConnection alloc] initWithRequest:theRequestFb delegate:self];
}



-(void)loginUser
{
    [ApplicationDelegate addProgressHUDToView:self.view];
    
    NSMutableDictionary *signInData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserSignInPostData]];
    
    [ApplicationDelegate.engine loginUserWithLoginDataDictionary:signInData CompletionHandler:^(NSMutableDictionary *responseDic) {
        self.emailTextView.text=@"";
        self.passwordTextView.text=@"";
        
        
        {
            if ([ApplicationDelegate isValid:responseDic])
            {
                
                if (responseDic.count>0)
                {

                    if ([[responseDic objectForKey:@"Message"]caseInsensitiveCompare:@"success"]==NSOrderedSame)
                    {
                        
               if( self.keepMeSignInBttn.selected)
               {
                   
            
             User *userObj = [ApplicationDelegate.mapper getUserObjectFromDictionary:[[responseDic objectForKey:@"users"] objectAtIndex:0]];
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            [prefs setObject:userObj.email forKey:@"Email"];
            [prefs setObject:userObj.firstname forKey:@"First Name"];
            [prefs setObject:userObj.languagekey forKey:@"Language Key"];
            [prefs setObject:userObj.lastName forKey:@"Last Name"];
            [prefs setObject:userObj.userId forKey:@"User Id"];
            [prefs setObject:userObj.mobilenumber forKey:@"Mobile Number"];
            [prefs setObject:@"login" forKey:@"loginflag"];
            [prefs synchronize];
               }
                        
               homePageViewObj = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
                  [self.navigationController pushViewController:homePageViewObj animated:NO];
                        
                        
                    }
                    
                    else
                    {
                        [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                    }
                    
                    [ApplicationDelegate removeProgressHUD];
  
                }
            
            }
        }
    } errorHandler:^(NSError *error) {
        
        [ApplicationDelegate removeProgressHUD];
        [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
        
    }];
    
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}


- (IBAction)forgetPassword:(id)sender {
    
   
    
    [ApplicationDelegate addProgressHUDToView:self.view];
    
    if(self.emailTextView.text.length != 0)
    {
    
    NSMutableDictionary *ForgetEmailData = [[NSMutableDictionary alloc] initWithDictionary:[self getForgetPasswordPostData]];
    
    
    [ApplicationDelegate.engine forgetPasswordDataDictionary:ForgetEmailData CompletionHandler:^(NSMutableDictionary *responseDictionary) {
        
        
        if ([ApplicationDelegate isValid:responseDictionary])
        {
            
            if (responseDictionary.count>0)
            {
        if ([responseDictionary objectForKey:@"Success"])
                {
        
       [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Message"] title:nil];
            
            [ApplicationDelegate removeProgressHUD];
                }
                
        else{
             [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Message"] title:nil];
        
        }
            
            }
        }
    
    } errorHandler:^(NSError *error) {
        
        [ApplicationDelegate removeProgressHUD];
        [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
    }];
        
    }
    
    
    else
    {
        
          [ApplicationDelegate showAlertWithMessage:@"Please Enter Email" title:nil];
        [ApplicationDelegate removeProgressHUD];
    }
    
     self.emailTextView.text=@"";
     
    
    

}


-(NSMutableDictionary *)getForgetPasswordPostData
{
    
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:self.emailTextView.text forKey:@"Email"];
    
    return postDic;
}

- (IBAction)passwordtext:(UITextField *)sender {
    self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    self.passwordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_SELECTED_C0lOR];
    self.emailIMg.image = [UIImage imageNamed:@"user-1.png"];
    self.passwordIMg.image = [UIImage imageNamed:@"password_sel-1.png"];
}


-(void)LoginwithFacebook
{
     NSMutableDictionary *FacebookUserData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserFacebookPostData]];
    
   [ApplicationDelegate.engine loginwithFacebookDataDictionary:FacebookUserData CompletionHandler:^(NSMutableDictionary *responseDictionary) {
       
        [ApplicationDelegate addProgressHUDToView:self.view];
       
       if ([ApplicationDelegate isValid:responseDictionary])
       {
       if (responseDictionary.count>0)
       {
       if ([responseDictionary objectForKey:@"Success"])
       {
           
           [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Message"] title:nil];
           
           [ApplicationDelegate removeProgressHUD];
           
           if( self.keepMeSignInBttn.selected)
           {
               
           NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
           
           [prefs setObject:@"login" forKey:@"loginflag"];
           
           [prefs synchronize];
           }
           
           homePageViewObj = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
           [self.navigationController pushViewController:homePageViewObj animated:NO];
           
       }
       
       else{
           [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Message"] title:nil];
           
           
           [ApplicationDelegate removeProgressHUD];
           
       }

       }
    
       }
   } errorHandler:^(NSError *error) {
       
       
       [ApplicationDelegate removeProgressHUD];
       [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
       
       
   }];
    
    
}

-(NSMutableDictionary *)getUserFacebookPostData
{
    
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:[self.postFbDic objectForKey:@"Email"] forKey:@"Email"];
    [postDic setObject:[self.postFbDic objectForKey:@"FirstName"] forKey:@"FirstName"];
    [postDic setObject:[self.postFbDic objectForKey:@"LastName"] forKey:@"LastName"];
    [postDic setObject:[self.postFbDic objectForKey:@"Id"] forKey:@"UserName"];
    NSLog(@"%@",postDic);
    
    return postDic;
}


-(void)LoginwithTwitter
{
    NSMutableDictionary *TwitterUserData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserTwitterPostData]];
    
    [ApplicationDelegate.engine loginwithTwitterDataDictionary:TwitterUserData CompletionHandler:^(NSMutableDictionary *responseDictionary) {
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        if ([ApplicationDelegate isValid:responseDictionary])
        {
            if (responseDictionary.count>0)
            {
                if ([responseDictionary objectForKey:@"Success"])
                {
                    
                    [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Message"] title:nil];
                    
                    [ApplicationDelegate removeProgressHUD];
                    
                    if( self.keepMeSignInBttn.selected)
                    {
                        
                    
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];

                    [prefs setObject:@"login" forKey:@"loginflag"];

                    [prefs synchronize];
                    
                    }
                    
                    homePageViewObj = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
                    [self.navigationController pushViewController:homePageViewObj animated:NO];
                    
                }
                
                else{
                    [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Message"] title:nil];
                    
                    
                    [ApplicationDelegate removeProgressHUD];
                    
                }
                
            }
            
        }
    } errorHandler:^(NSError *error) {
        
        
        [ApplicationDelegate removeProgressHUD];
        [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
        
        
    }];
    
    
}

-(NSMutableDictionary *)getUserTwitterPostData
{
    
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];

    [postDic setObject:[self.postTweetDic objectForKey:@"FirstName"] forKey:@"FirstName"];
    [postDic setObject:[self.postTweetDic objectForKey:@"Id"] forKey:@"UserName"];
    
    return postDic;
}




@end
