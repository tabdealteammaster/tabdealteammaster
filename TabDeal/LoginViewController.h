//
//  LoginViewController.h
//  TabDeal
//
//  Created by satheesh on 9/17/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <Twitter/Twitter.h>
#import "HomeViewController.h"
#import "User.h"
@interface LoginViewController : UIViewController<FBLoginViewDelegate,UITextFieldDelegate>{
    NSMutableURLRequest * theRequest;
    NSURL * url;
    NSMutableURLRequest * theRequestFb;
    NSURL * urlFb;
    UIAlertView * alrtView;
     NSUserDefaults * userDataStore;
    
}
@property (weak, nonatomic) IBOutlet UIView *LoginmainBackView;
@property (weak, nonatomic) IBOutlet UILabel *emailHeadingLbl;
@property(strong , nonatomic)NSMutableDictionary *postTweetDic;
@property (weak, nonatomic) IBOutlet UIImageView *loginBackView;
@property (weak, nonatomic) IBOutlet UIImageView *emailBackView;
@property (weak, nonatomic) IBOutlet UIImageView *passwordBackView;
@property (weak, nonatomic) IBOutlet UIButton *loginBttn;
@property (weak, nonatomic) IBOutlet UIButton *registerBttn;
@property (weak, nonatomic) IBOutlet UIView *twitterBackVIew;
@property (weak, nonatomic) IBOutlet FBLoginView *signinwithFBBttn;
- (IBAction)registerBtnActn:(UIButton *)sender;
- (IBAction)loginBttnActn:(UIButton *)sender;
- (IBAction)keepMeSignedInBttnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *keepMeSignInBttn;
@property (weak, nonatomic) IBOutlet UITextField *emailTextView;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextView;
- (IBAction)emailtext:(UITextField *)sender;
@property (strong , nonatomic)NSURLConnection * connection;
@property( strong , nonatomic)NSMutableData * receivedData;
@property (strong , nonatomic)NSURLConnection * connectionFb;
@property( strong , nonatomic)NSMutableData * receivedDataFb;
@property(strong , nonatomic)NSMutableDictionary *postFbDic;
- (IBAction)passwordtext:(UITextField *)sender;
@property (weak, nonatomic) IBOutlet UILabel *passwordHeadingLbl;
@property (weak, nonatomic) IBOutlet UILabel *logInToYourAccountLbl;
@property (strong, nonatomic) IBOutlet UILabel *EmailLabel;
- (IBAction)forgetPassword:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordBtn;
@property (weak, nonatomic) IBOutlet UILabel *keepMeSignInLbl;
@property (weak, nonatomic) IBOutlet UIImageView *emailIMg;
@property (weak, nonatomic) IBOutlet UIImageView *passwordIMg;

@end
