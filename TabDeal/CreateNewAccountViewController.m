//
//  CreateNewAccountViewController.m
//  TabDeal
//
//  Created by satheesh on 9/17/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "CreateNewAccountViewController.h"

@interface CreateNewAccountViewController (){
    LoginViewController * loginViewObj;
}

@end

@implementation CreateNewAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    self.mobileNumberTextView.inputAccessoryView = numberToolbar;
    

    UIToolbar* numberToolbarCode = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbarCode.barStyle = UIBarStyleBlackTranslucent;
    numberToolbarCode.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPadCode)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPadCode)]];
    [numberToolbarCode sizeToFit];
    self.codeTextView.inputAccessoryView = numberToolbarCode;
    
    
    self.createNewBackView.layer.cornerRadius = 4;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.mobileNumberTextView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.codeTextView];
    
    self.firstBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    self.lastBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    self.passwordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    self.confirmPasswordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    self.mobileNumBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    
    self.navigationController.navigationBarHidden = YES;
    self.createNewBackView.layer.cornerRadius = 4;
    self.firstBackView.layer.cornerRadius = 3;
    self.lastBackView.layer.cornerRadius = 3;
    self.emailBackView.layer.cornerRadius = 3;
    self.passwordBackView.layer.cornerRadius = 3;
    self.confirmPasswordBackView.layer.cornerRadius = 3;
    self.mobileNumBackView.layer.cornerRadius = 3;
    // Do any additional setup after loading the view from its nib.
    
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)signUpBtnAction:(UIButton *)sender {
    
    

    
    
    if ((self.firstNameTextView.text.length == 0) && (self.lastNameTextView.text.length == 0) && (self.emailTextView.text.length == 0) && (self.passwordTextView.text.length == 0) && (self.confirmPassTextView.text.length == 0) &&(self.mobileNumberTextView.text.length == 0)) {
        alrtView = [[UIAlertView alloc]initWithTitle:@"Sorry..!" message:@"Please enter your details" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alrtView show];
    } else if(self.firstNameTextView.text.length == 0){
        alrtView = [[UIAlertView alloc]initWithTitle:@"Sorry..!" message:@"Please enter your First name" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alrtView show];
    }
//    else if(self.lastNameTextView.text.length == 0){
//        alrtView = [[UIAlertView alloc]initWithTitle:@"Sorry..!" message:@"Please enter your Last name" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
//        [alrtView show];
//    }
    else if(self.emailTextView.text.length == 0){
        alrtView = [[UIAlertView alloc]initWithTitle:@"Sorry..!" message:@"Please enter your email id" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alrtView show];
    }
    else if(self.passwordTextView.text.length == 0){
        alrtView = [[UIAlertView alloc]initWithTitle:@"Sorry..!" message:@"Please enter a password" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alrtView show];
    }
//    else if(self.mobileNumberTextView.text.length == 0){
//        alrtView = [[UIAlertView alloc]initWithTitle:@"Sorry..!" message:@"Please enter your mobile number" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
//        [alrtView show];
//    }
    else{
        if (![self NSStringIsValidEmail:self.emailTextView.text]) {
            alrtView = [[UIAlertView alloc]initWithTitle:@"Sorry..!" message:@"Please enter a valid email id" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alrtView show];
        }else if(![self.passwordTextView.text isEqualToString:self.confirmPassTextView.text]){
            alrtView = [[UIAlertView alloc]initWithTitle:@"Sorry..!" message:@"Confirmation password is wrong" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alrtView show];
        }
//        else if(![self validatePhone:self.mobileNumberTextView.text]){
//            alrtView = [[UIAlertView alloc]initWithTitle:@"Sorry..!" message:@"Please enter a valid phone number" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
//            [alrtView show];
//        }
        else{
            [self registerUser];

        }
    }
    
}

- (IBAction)backBttnActn:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

-(NSMutableDictionary *)getUserSignUpPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
      [postDic setObject:self.firstNameTextView.text forKey:@"FirstName"];
    [postDic setObject:self.lastNameTextView.text forKey:@"LastName"];
    [postDic setObject:self.emailTextView.text forKey:@"Email"];
     [postDic setObject:self.passwordTextView.text forKey:@"Password"];
     [postDic setObject:self.mobileNumberTextView.text forKey:@"MobileNumber"];
    [postDic setObject:@"123456" forKey:@"LanguageKey"];
     return postDic;
}


#pragma NSURLConnection Delegates



-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (BOOL)validatePhone:(NSString *)phoneNumber
{
    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:phoneNumber];
}


- (IBAction)textViewDidSelActn:(UITextField *)sender {
    if (sender.tag == 1) {
        self.firstBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_SELECTED_C0lOR];
        self.lastBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.passwordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.confirmPasswordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.mobileNumBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    }
    else if (sender.tag == 2){
        self.firstBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.lastBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_SELECTED_C0lOR];
        self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.passwordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.confirmPasswordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.mobileNumBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    }
    else if (sender.tag == 3){
        self.firstBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.lastBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_SELECTED_C0lOR];
        self.passwordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.confirmPasswordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.mobileNumBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    }
    else if (sender.tag == 4){
        self.firstBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.lastBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.passwordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_SELECTED_C0lOR];
        self.confirmPasswordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.mobileNumBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    }
    else if (sender.tag == 5){
        self.firstBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.lastBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.passwordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.confirmPasswordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_SELECTED_C0lOR];
        self.mobileNumBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    }
    else if (sender.tag == 6){
        self.firstBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.lastBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.passwordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.confirmPasswordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.mobileNumBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_SELECTED_C0lOR];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    self.firstNameTextView.text = nil;
    self.lastNameTextView.text = nil;
    self.emailTextView.text = nil;
    self.passwordTextView.text = nil;
    self.confirmPassTextView.text = nil;
    self.mobileNumberTextView.text = nil;
    if (buttonIndex == 0)
    {
        //Code for OK button
    }
    if (buttonIndex == 1)
    {
        //Code for download button
    }
    alrtView.tag = 0;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

-(void)registerUser
{
    NSMutableDictionary *signUpData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserSignUpPostData]];
    
    
    if (signUpData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        
        
        [ApplicationDelegate.engine RegisterUserWithPostDataDictionary:signUpData CompletionHandler:^(NSMutableDictionary *responseDic)
         
         {
             
             self.firstNameTextView.text=@"";
             self.lastNameTextView.text=@"";
             self.emailTextView.text=@"";
             self.passwordTextView.text=@"";
             self.confirmPassTextView.text=@"";
             self.mobileNumberTextView.text=@"";
             
             if ([ApplicationDelegate isValid:responseDic])
             {
                 
                 if (responseDic.count>0)
                 {
                     
                     if ([responseDic objectForKey:@"Success"])
                     {
                         
                         
                         [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:@"Message"];
                         
                         loginViewObj = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
                         [self.navigationController pushViewController:loginViewObj animated:NO];
                         
                         
                         [ApplicationDelegate removeProgressHUD];
                         
                     }
                     
                     else
                     {
                         [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                     }
                     
                     
                     
                 }
             }
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
             
         }];
        
    }
}

- (void)limitTextField:(NSNotification *)note {
    int limit = 10;
    int codeLimit = 3;
    if (self.mobileNumberTextView.text.length > limit) {
        [self.mobileNumberTextView setText:[self.mobileNumberTextView.text substringToIndex:limit]];
    }else if(self.codeTextView.text.length > codeLimit){
        [self.codeTextView setText:[self.codeTextView.text substringToIndex:codeLimit]];
    }
}

-(void)cancelNumberPad{
    [ self.mobileNumberTextView resignFirstResponder];
     self.mobileNumberTextView.text = @"";
}

-(void)doneWithNumberPad{
   // NSString *numberFromTheKeyboard =  self.mobileNumberTextView.text;
    [self.mobileNumberTextView resignFirstResponder];
}

-(void)cancelNumberPadCode{
    [ self.codeTextView resignFirstResponder];
    self.codeTextView.text = @"";
}

-(void)doneWithNumberPadCode{
    // NSString *numberFromTheKeyboard =  self.mobileNumberTextView.text;
    [self.codeTextView resignFirstResponder];
}

@end
