//
//  ObjectMapper.m
//  TabDeal
//
//  Created by Sreeraj VR on 25/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "ObjectMapper.h"

@implementation ObjectMapper


-(User *)getUserObjectFromDictionary:(NSMutableDictionary *)userDictionary
{
    User *usr = [[User alloc] init];
    
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"Email"]])
    {
        usr.email = [NSString stringWithFormat:@"%@",[userDictionary objectForKey:@"Email"]];
    }
    else
    {
        usr.email = @"";
    }
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"FirstName"]])
    {
        usr.firstname = [userDictionary objectForKey:@"FirstName"];
    }
    else
    {
        usr.firstname = @"";
    }
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"LanguageKey"]])
    {
        usr.languagekey = [userDictionary objectForKey:@"LanguageKey"];
    }
    else
    {
        usr.languagekey = @"";
    }
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"LastName"]])
    {
        usr.lastName = [userDictionary objectForKey:@"LastName"];
    }
    else
    {
        usr.lastName = @"";
    }
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"MobileNumber"]])
    {
        usr.mobilenumber = [userDictionary objectForKey:@"MobileNumber"];
    }
    else
    {
        usr.mobilenumber = @"";
    }
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"UserId"]])
    {
        usr.userId = [userDictionary objectForKey:@"UserId"];
    }
    else
    {
        usr.userId = @"";
    }
    
    return usr;
}


@end
