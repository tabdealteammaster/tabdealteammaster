//
//  AppEngine.h
//  TabDeal
//
//  Created by Sreeraj VR on 21/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "MKNetworkEngine.h"

@interface AppEngine : MKNetworkEngine
typedef void (^ResponseDictionaryBlock)(NSMutableDictionary* responseDictionary);
typedef void (^ResponseArrayBlock)(NSMutableArray* responseArray);


-(void)loginUserWithLoginDataDictionary:(NSMutableDictionary *)logDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)RegisterUserWithPostDataDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;


-(void)forgetPasswordDataDictionary:(NSMutableDictionary *)forDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)loginwithFacebookDataDictionary:(NSMutableDictionary *)logDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)loginwithTwitterDataDictionary:(NSMutableDictionary *)logDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

@end
