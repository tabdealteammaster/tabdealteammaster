//
//  ObjectMapper.h
//  TabDeal
//
//  Created by Sreeraj VR on 25/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface ObjectMapper : NSObject

-(User *)getUserObjectFromDictionary:(NSMutableDictionary *)userDictionary;

@end
