//
//  HomeViewController.m
//  TabDeal
//
//  Created by Sreeraj VR on 17/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "HomeViewController.h"
#import "NewDealsViewController.h"
#import "BuyViewController.h"
#import "SellViewController.h"
#import "SearchViewController.h"
#import "AboutTabDealViewController.h"
#import "MyAccountViewController.h"
#import "FavouriteItemsViewController.h"
#import "PreviousOrdersViewController.h"
#import "SubmittedItemsViewController.h"
#import "NotificationsViewController.h"
#import "LogoutViewController.h"

static HomeViewController *sharedObj = NULL;

@interface HomeViewController ()

@end

@implementation HomeViewController
{
    NSArray *moreMenuListNameArray;
    
    BOOL moreMenuItemSelectionFlag;
    NSUInteger moreItemSelectedIndex;
    NSUInteger tabSelectIndex;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
     self.homeButton.selected=YES;
    
    moreMenuListNameArray = [[NSArray alloc] initWithObjects:@"About Tabdeal",@"My Account",@"Favourite Items",@"Previous Orders",@"Submitted Items",@"Notifications",@"Log Out", nil];
    [self setUpInitialView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)homeTabButtonAction:(UIButton *)sender
{
    [self didSelectTabItemAtIndex:sender.tag];
}

-(void)showMoreMenu
{
  
 
    [self.homeTabContainerView setUserInteractionEnabled:NO];
    float xFact,moreMenuViewHeight;
    
    if ([UIScreen mainScreen].bounds.size.height==480)
    {
        moreMenuViewHeight = 295.0f;
    }
    else
    {
        moreMenuViewHeight = 392.0f;
    }
    
    float xPos = - self.moreMenuView.frame.size.width;
   
        xPos = self.view.frame.size.width;
    
    
    self.moreMenuView.frame = CGRectMake(xPos, (self.view.frame.size.height-moreMenuViewHeight-self.footerTabView.frame.size.height-1), self.moreMenuView.frame.size.width, moreMenuViewHeight);
    
    
    xFact = 0;
    
        xFact=self.view.frame.size.width-self.moreMenuView.frame.size.width;
    
    
    self.moreMenuTable.delegate=self;
    self.moreMenuTable.dataSource=self;
    
    [self.view insertSubview:self.moreMenuView atIndex:10];
    
    
    [UIView beginAnimations:@"animateView" context:nil];
    [UIView setAnimationDuration:0.5];
    CGRect viewFrame = [self.moreMenuView frame];
    viewFrame.origin.x  = xFact;
    self.moreMenuView.frame = viewFrame;

    [UIView commitAnimations];
    [self.moreMenuTable reloadData];
    
}
-(void)hideMoreMenu
{
  [self.homeTabContainerView setUserInteractionEnabled:YES];
    
    [UIView beginAnimations:@"animateView" context:nil];
    CGRect viewFrame = [self.moreMenuView frame];
    
    viewFrame.origin.x = - self.moreMenuView.frame.size.width;
   
        viewFrame.origin.x  = self.view.frame.size.width;
    
    [UIView setAnimationDuration:0.5];
    self.moreMenuView.frame = viewFrame;
    [UIView commitAnimations];
    [self performSelector:@selector(removeMoreMenu) withObject:nil afterDelay:0.5];
}
-(void)removeMoreMenu
{
    [self.moreMenuView removeFromSuperview];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/




-(void)didSelectTabItemAtIndex:(NSInteger)itemIndex
{
    [self resetTabSelection];
    
    if (itemIndex==5)
    {
        self.moreButton.selected=YES;
        if ([self.moreMenuView superview])
        {
            [self hideMoreMenu];
        }
        else
        {
            [self showMoreMenu];
        }
    }
    else
    {
        moreMenuItemSelectionFlag = NO;
        if ([self.moreMenuView superview])
        {
            [self hideMoreMenu];
        }
        
        switch (itemIndex)
        {
            case 1:
            {
                self.homeButton.selected = YES;
                [self loadHomeView];
            }
                break;
            case 2:
            {
                self.buyButton.selected = YES;
                [self loadBuyView];
            }
                break;
            case 3:
            {
                self.sellButton.selected = YES;
                [self loadSellView];
            }
                break;
            case 4:
            {
                self.searchButton.selected = YES;
                [self loadSearchView];
            }
                break;
            default:
                break;
        }
        
    }
    
}

-(void)resetTabSelection
{
    self.homeButton.selected=NO;
    self.searchButton.selected=NO;
    self.buyButton.selected=NO;
    self.sellButton.selected=NO;
    self.moreButton.selected=NO;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return moreMenuListNameArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([UIScreen mainScreen].bounds.size.height==480)
    {
        return 42;
    }
    else
    {
        return 48;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        // Configure the cell...
        
        cell.selectionStyle = UITableViewCellAccessoryNone;
       cell.backgroundColor = [UIColor clearColor];
        
    //    cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"more_divider.png"]];
        
        cell.backgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"more_divider.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
        
        cell.contentView.backgroundColor = [UIColor clearColor];
        
        UIImageView *iconImgView;
        UILabel *nameLabel;
        
        if ([UIScreen mainScreen].bounds.size.height==480)
        {
            
            iconImgView= [[UIImageView alloc] initWithFrame:CGRectMake(12, 9, 26, 26)];
            nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(48, 9, (tableView.frame.size.width-58), 26)];
            
        }
        else
        {
            iconImgView= [[UIImageView alloc] initWithFrame:CGRectMake(10, 8, 38, 38)];
            nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(56, 8, (tableView.frame.size.width-63), 38)];
            
        }
        
        
        iconImgView.backgroundColor = [UIColor clearColor];
        iconImgView.tag=50;
        nameLabel.tag=60;
        [cell addSubview:iconImgView];
        [cell addSubview:nameLabel];
    }
    

    
    UILabel* cellLabel = (UILabel*)[cell viewWithTag: 60];
    [cellLabel setBackgroundColor:[UIColor clearColor]];
    cellLabel.text = [self getMoreTabItemTitle:indexPath.row];
   
    if ([UIScreen mainScreen].bounds.size.height==480)
    {
       
        float xLabelPos = 0.0f;
        cellLabel.textAlignment = NSTextAlignmentLeft;

        cellLabel.frame = CGRectMake(xLabelPos, 9, (tableView.frame.size.width-58), 26);
        
    }
    else
    {
    
        float xLabelPos = 0.0f;
        cellLabel.textAlignment = NSTextAlignmentLeft;
        cellLabel.frame = CGRectMake(xLabelPos, 12, (tableView.frame.size.width-60), 30);
        
    }
    
    
    if (moreMenuItemSelectionFlag &&(moreItemSelectedIndex==indexPath.row))
    {

         cellLabel.textColor=[UIColor redColor];
        
    }
    else
    {

        cellLabel.textColor=[UIColor colorWithHex:kUNIVERSAL_THEME_COLOR];
    }
    
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    moreItemSelectedIndex=indexPath.row;
    moreMenuItemSelectionFlag = YES;
    
    [self selectMoreMenuItem:indexPath.row];
    [self.moreMenuTable reloadData];
}

-(NSString *)getMoreTabItemTitle:(NSUInteger)itemIndex
{
    NSString *titleString=@"";
    titleString = [moreMenuListNameArray objectAtIndex:itemIndex];
    
    return titleString;
}




-(void)selectMoreMenuItem:(NSInteger)index
{

    
    [self hideMoreMenu];
    

    switch (index)
    {
        case 0:
        {
            [self loadaboutView];
            
        }
            break;
        case 1:
        {
            [self loadmyAccountView];

        }
            break;
        case 2:
        {

            [self loadFavouriteView];
            
        }
            break;
        case 3:
        {
 
            [self loadPreviousOrdersView];
            
        }
            break;
        case 4:
        {
            
            [self loadSubmittedItemsView];

        }
            break;
            
        case 5:
        {
            [self loadNotificationsView];
            
        }
            break;
            
        case 6:
        {
            [self loadLogOutView];
            
        }
            break;
            
            
        default:
            break;
    }
    
    
}

+(HomeViewController*)sharedViewController
{
    if ( !sharedObj || sharedObj == NULL )
    {
        NSString *nibName = @"HomeViewController";
               sharedObj = [[HomeViewController alloc] initWithNibName:nibName bundle:nil];
    }
    
    return sharedObj;

}


-(void)setUpInitialView
{
    
    for (UIView *vw in self.homeTabContainerView.subviews)
    {
        [vw removeFromSuperview];
    }
    
    NewDealsViewController *newDealsDetailVc = [[NewDealsViewController alloc] initWithNibName:@"NewDealsViewController" bundle:nil];
    newDealsDetailVc.view.backgroundColor = [UIColor clearColor];
    
    ApplicationDelegate.homeTabNav=[[UINavigationController alloc] initWithRootViewController:newDealsDetailVc];
    
    ApplicationDelegate.homeTabNav.navigationBar.translucent = NO;
    ApplicationDelegate.homeTabNav.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.homeTabNav.navigationBarHidden=YES;

ApplicationDelegate.homeTabNav.view.frame=CGRectMake(0, 0, self.homeTabContainerView.frame.size.width, self.homeTabContainerView.frame.size.height);

    [self.homeTabContainerView addSubview:ApplicationDelegate.homeTabNav.view];

}


-(void)loadHomeView
{
    NewDealsViewController *dealsVc = [[NewDealsViewController alloc] initWithNibName:@"NewDealsViewController" bundle:nil];
    dealsVc.view.backgroundColor = [UIColor clearColor];

    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[dealsVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:dealsVc animated:NO];
    }
    
}





-(void)loadBuyView
{
    BuyViewController *buyVc = [[BuyViewController alloc] initWithNibName:@"BuyViewController" bundle:nil];
    buyVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[buyVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:buyVc animated:NO];
    }
}

-(void)loadSellView
{
    SellViewController *sellVc = [[SellViewController alloc] initWithNibName:@"SellViewController" bundle:nil];
    sellVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[sellVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:sellVc animated:NO];
    }
}

-(void)loadSearchView
{
    SearchViewController *searchVc = [[SearchViewController alloc] initWithNibName:@"SearchViewController" bundle:nil];
    searchVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[searchVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:searchVc animated:NO];
    }
}


-(void) loadaboutView
{
    AboutTabDealViewController *aboutVc = [[AboutTabDealViewController alloc] initWithNibName:@"AboutTabDealViewController" bundle:nil];
    aboutVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[aboutVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:aboutVc animated:NO];
    }
}


-(void) loadmyAccountView
{
    MyAccountViewController *myaccountVc = [[MyAccountViewController alloc] initWithNibName:@"MyAccountViewController" bundle:nil];
    myaccountVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[myaccountVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:myaccountVc animated:NO];
    }
}


-(void) loadFavouriteView
{
    FavouriteItemsViewController *FavViewVc = [[FavouriteItemsViewController alloc] initWithNibName:@"FavouriteItemsViewController" bundle:nil];
    FavViewVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[FavViewVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:FavViewVc animated:NO];
    }
}

-(void) loadPreviousOrdersView
{
    PreviousOrdersViewController *PreviousViewVc = [[PreviousOrdersViewController alloc] initWithNibName:@"PreviousOrdersViewController" bundle:nil];
    PreviousViewVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[PreviousViewVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:PreviousViewVc animated:NO];
    }
}

-(void) loadSubmittedItemsView
{
    SubmittedItemsViewController *SubmittedViewVc = [[SubmittedItemsViewController alloc] initWithNibName:@"SubmittedItemsViewController" bundle:nil];
    SubmittedViewVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[SubmittedViewVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:SubmittedViewVc animated:NO];
    }
}


-(void) loadNotificationsView
{
    NotificationsViewController *NotificationViewVc = [[NotificationsViewController alloc] initWithNibName:@"NotificationsViewController" bundle:nil];
    NotificationViewVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[NotificationViewVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:NotificationViewVc animated:NO];
    }
}

-(void) loadLogOutView
{
    LogoutViewController *LogoutViewVc = [[LogoutViewController alloc] initWithNibName:@"LogoutViewController" bundle:nil];
    LogoutViewVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[LogoutViewVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:LogoutViewVc animated:NO];
    }
}
@end
