//
//  AppDelegate.h
//  TabDeal
//
//  Created by Sreeraj VR on 17/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LaunchingViewController.h"
#import "HomeViewController.h"
#import "AppEngine.h"
#import "UserData.h"
#import "Reachability.h"
#import "MBProgressHUD.h"
#import "ObjectMapper.h"

#define ApplicationDelegate ((AppDelegate *)[UIApplication sharedApplication].delegate)

@interface AppDelegate : UIResponder <UIApplicationDelegate,MBProgressHUDDelegate>
{
     Reachability* internetReachable;
}

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic)LaunchingViewController *launchViewObj;

@property (strong, nonatomic) AppEngine *engine;

@property(strong,nonatomic) MBProgressHUD *HUD;

@property (strong, nonatomic)UINavigationController *homeTabNav;

@property (strong, nonatomic) ObjectMapper *mapper;
//@property (strong, nonatomic)ObjectMapper *mapper;

-(BOOL)isValid:(id)sender;
-(BOOL)checkNetworkAvailability;
-(void)showAlertWithMessage:(NSString *)message title:(NSString *)title;
-(void)addProgressHUDToView:(UIView *)parentview;
-(void)removeProgressHUD;

@end

