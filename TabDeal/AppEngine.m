//
//  AppEngine.m
//  TabDeal
//
//  Created by Sreeraj VR on 21/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "AppEngine.h"
#import "SBJson.h"

@implementation AppEngine

-(void)RegisterUserWithPostDataDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kSignUp_URL params:postDic httpMethod:@"POST"];
    
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    

     [header setValue:@"application/json; charset=utf-8" forKey:@"Content-Type"];
    
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];

    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"signup_ststus"]);
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}


-(void)loginUserWithLoginDataDictionary:(NSMutableDictionary *)logDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kSignIn_URL params:logDic httpMethod:@"POST"];
    

    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];

    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];

    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"list"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}


-(void)forgetPasswordDataDictionary:(NSMutableDictionary *)forDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kForgetPassword_URL params:forDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"list"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
    
}

-(void)loginwithFacebookDataDictionary:(NSMutableDictionary *)logDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kFacebookSignIn_URL params:logDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"list"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
    
}



-(void)loginwithTwitterDataDictionary:(NSMutableDictionary *)logDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kTwitterSignIn_URL params:logDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"list"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
    
}

@end
