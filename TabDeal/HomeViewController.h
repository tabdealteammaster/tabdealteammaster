//
//  HomeViewController.h
//  TabDeal
//
//  Created by Sreeraj VR on 17/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TD_Constants.h"


@interface HomeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UIView *footerTabView;
@property (strong, nonatomic) IBOutlet UITableView *moreMenuTable;
@property (strong, nonatomic) IBOutlet UIView *moreMenuView;

- (IBAction)homeTabButtonAction:(UIButton *)sender;
+(HomeViewController*)sharedViewController;

@property (strong, nonatomic) IBOutlet UIButton *homeButton;

@property (strong, nonatomic) IBOutlet UIButton *buyButton;
@property (strong, nonatomic) IBOutlet UIButton *sellButton;

@property (strong, nonatomic) IBOutlet UIButton *searchButton;
@property (strong, nonatomic) IBOutlet UIButton *moreButton;
@property (weak, nonatomic) IBOutlet UIView *homeTabContainerView;

@end
