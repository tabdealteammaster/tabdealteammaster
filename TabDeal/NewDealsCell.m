//
//  NewDealsCell.m
//  TabDeal
//
//  Created by Sreeraj VR on 29/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "NewDealsCell.h"

@implementation NewDealsCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        NSString *nib = @"NewDealsCell";
        NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
        self = (NewDealsCell*)[nibViews objectAtIndex: 0];
    }
    return self;
}



@end
