//
//  TD_Constants.h
//  TabDeal
//
//  Created by Sreeraj VR on 17/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#ifndef TabDeal_TD_Constants_h
#define TabDeal_TD_Constants_h

#define kUNIVERSAL_THEME_COLOR 0xc88f22
#define kUNIVERSAL_SELECTED_C0lOR 0xB87BBF
#define kUNIVERSAL_UNSELECTED_C0lOR 0xDDE0E1

#define kHOST_URL @"tabdeal.mawaqaademo.com/service/API.svc"
#define kSignUp_URL @"SignUp"
#define kSignIn_URL @"SignIn"
#define kForgetPassword_URL @"FogotPassword"
#define kFacebookSignIn_URL @"FacebookSignIn"
#define kTwitterSignIn_URL @"TwitterSignIn"
#define kSERVER_ERROR_MSG @"Server error occurred. Please try later."
#define kERROR_MSG @"Something went wrong. Please try later."

#define kFAILED_ERROR_MSG @"Request submission failed. Please try later."

#endif
